use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};
use std::{convert::Infallible, io::Write, path::PathBuf};

fn run_inference(user_prompt: String) -> Result<String, Box<dyn std::error::Error>> {
    let tokenizer = llm::TokenizerSource::Embedded;
    let model_type = llm::ModelArchitecture::GptNeoX;
    let model_location = PathBuf::from("src/pythia-1b-q4_0-ggjt.bin");

    let processed_prompt = user_prompt.to_string();
    let model = llm::load_dynamic(
        Some(model_type),
        &model_location,
        tokenizer,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;

    let mut session = model.start_session(Default::default());
    let mut tokens_generated = String::new();

    let inference_result = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &llm::InferenceRequest {
            prompt: (&processed_prompt).into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(20),
        },
        &mut Default::default(),
        |response| match response {
            llm::InferenceResponse::PromptToken(token) | llm::InferenceResponse::InferredToken(token) => {
                print!("{token}");
                std::io::stdout().flush().unwrap();
                tokens_generated.push_str(&token);
                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Continue),
        },
    );

    match inference_result {
        Ok(_) => Ok(tokens_generated),
        Err(error) => Err(Box::new(error)),
    }
}

async fn handle_request(req: Request) -> Result<Response<Body>, Error> {
    let query_param = req
        .query_string_parameters_ref()
        .and_then(|params| params.first("query"))
        .unwrap_or("I like Duke University because");
    let result_message = match run_inference(query_param.to_string()) {
        Ok(result) => format!("{}", result),
        Err(error) => format!("Error in inference: {:?}", error),
    };
    println!("Model generated response");
    println!("{:?}", result_message);

    let response = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(result_message.into())
        .map_err(Box::new)?;
    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(handle_request)).await
}
