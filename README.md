# Mini Project 10
This initiative is centered on developing a serverless inference endpoint with AWS Lambda, employing the Rust programming language and Cargo Lambda for deployment. The core of the project lies in integrating a lower-memory llm within AWS Lambda's constraints, specifically adhering to the 3GB memory cap for new AWS users. Although AWS Lambda can support up to 10GB, this restriction prolongs inference times. The `rustformers` library is utilized to import a compressed model from Hugging Face, demonstrating the capability and performance within the stipulated memory limitations.

## Description

For the serverless AWS Lambda function built using Rust and Cargo Lambda, the default inference query is pre-set to "I like Duke University because". To engage with the inference service and obtain customized outputs, you would invoke the AWS Lambda function with a personalized query. For instance, by providing the prompt "I like Chelsea football club because" as input to the Lambda function, the service leverages the rustformers library to generate a completion of the sentence. This allows for the exploration of diverse responses that the model can offer, all within the operational memory limits set for new AWS accounts.

### Output
For default query:
```txt
I like Duke University because it's a hub of knowledge and innovation. It help students succeed in
```

For example query:
```txt
I like Chelsea Football Club because they play with heart and skill. It brings
```



## Steps
1. Run `cargo lambda new miniproject10`
2. Choose a Hugging Face model with `rustformers`
3. Modify the template with your own additions or deletions
4. Implement an inference endpoint using the `rustfomers` package
5. Add needed dependencies to `Cargo.toml`
6. Run `cargo lambda watch` to test the set up locally
7. Navigate to the AWS IAM Management Console to create a new IAM user
8. Attach policies `lambdafullaccess` and `iamfullaccess` to this user
9. Configure your environment with your AWS credentials by exporting `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`
10. Navigate to AWS Elastc Container Registry and create a new private repository
11. Run `docker buildx build --progress=plain --platform linux/arm64 -t <your image name here> .` to build a container image with specific configurations by Docker
12. Navidate to AWS Lambda to create a new function
13. Deploy with the image selecting the `arm64` architecture.
15. After deploy, go to `Configuration` and then `General Configuration` to change memory and timeout to max
16. Next, go to `Configuration` and then `Function URL` to create a new function URL and enable CORS
17. Test the fucntion URL

